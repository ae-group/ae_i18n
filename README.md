<!-- THIS FILE IS EXCLUSIVELY MAINTAINED by the project ae.ae V0.3.95 -->
<!-- THIS FILE IS EXCLUSIVELY MAINTAINED by the project aedev.tpl_namespace_root V0.3.14 -->
# i18n 0.3.32

[![GitLab develop](https://img.shields.io/gitlab/pipeline/ae-group/ae_i18n/develop?logo=python)](
    https://gitlab.com/ae-group/ae_i18n)
[![LatestPyPIrelease](
    https://img.shields.io/gitlab/pipeline/ae-group/ae_i18n/release0.3.31?logo=python)](
    https://gitlab.com/ae-group/ae_i18n/-/tree/release0.3.31)
[![PyPIVersions](https://img.shields.io/pypi/v/ae_i18n)](
    https://pypi.org/project/ae-i18n/#history)

>ae_i18n package 0.3.32.

[![Coverage](https://ae-group.gitlab.io/ae_i18n/coverage.svg)](
    https://ae-group.gitlab.io/ae_i18n/coverage/index.html)
[![MyPyPrecision](https://ae-group.gitlab.io/ae_i18n/mypy.svg)](
    https://ae-group.gitlab.io/ae_i18n/lineprecision.txt)
[![PyLintScore](https://ae-group.gitlab.io/ae_i18n/pylint.svg)](
    https://ae-group.gitlab.io/ae_i18n/pylint.log)

[![PyPIImplementation](https://img.shields.io/pypi/implementation/ae_i18n)](
    https://gitlab.com/ae-group/ae_i18n/)
[![PyPIPyVersions](https://img.shields.io/pypi/pyversions/ae_i18n)](
    https://gitlab.com/ae-group/ae_i18n/)
[![PyPIWheel](https://img.shields.io/pypi/wheel/ae_i18n)](
    https://gitlab.com/ae-group/ae_i18n/)
[![PyPIFormat](https://img.shields.io/pypi/format/ae_i18n)](
    https://pypi.org/project/ae-i18n/)
[![PyPILicense](https://img.shields.io/pypi/l/ae_i18n)](
    https://gitlab.com/ae-group/ae_i18n/-/blob/develop/LICENSE.md)
[![PyPIStatus](https://img.shields.io/pypi/status/ae_i18n)](
    https://libraries.io/pypi/ae-i18n)
[![PyPIDownloads](https://img.shields.io/pypi/dm/ae_i18n)](
    https://pypi.org/project/ae-i18n/#files)


## installation


execute the following command to install the
ae.i18n package
in the currently active virtual environment:
 
```shell script
pip install ae-i18n
```

if you want to contribute to this portion then first fork
[the ae_i18n repository at GitLab](
https://gitlab.com/ae-group/ae_i18n "ae.i18n code repository").
after that pull it to your machine and finally execute the
following command in the root folder of this repository
(ae_i18n):

```shell script
pip install -e .[dev]
```

the last command will install this package portion, along with the tools you need
to develop and run tests or to extend the portion documentation. to contribute only to the unit tests or to the
documentation of this portion, replace the setup extras key `dev` in the above command with `tests` or `docs`
respectively.

more detailed explanations on how to contribute to this project
[are available here](
https://gitlab.com/ae-group/ae_i18n/-/blob/develop/CONTRIBUTING.rst)


## namespace portion documentation

information on the features and usage of this portion are available at
[ReadTheDocs](
https://ae.readthedocs.io/en/latest/_autosummary/ae.i18n.html
"ae_i18n documentation").
